import org.sql2o.Sql2o
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import javax.servlet.ServletConfig
import javax.servlet.annotation.WebServlet
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebServlet(name = "FileServlet", urlPatterns = ["/download/*"], loadOnStartup = 1)
class FileServlet : HttpServlet() {

    private lateinit var fileRepo: FileInfoRepository
    private lateinit var dataDir: Path

    override fun init(config: ServletConfig) {
        super.init(config)
        val connStr = config.getInitParameter("db_connection")
        val dbUsername = config.getInitParameter("db_username")
        val dbPasswd = config.getInitParameter("db_password")
        val db = Sql2o(connStr, dbUsername, dbPasswd, Sql2oQuirks())
        val connProvider = Sql2oConnectionProvider(db)
        fileRepo = Sql2oFileInfoRepository(connProvider)

        dataDir = Paths.get(config.getInitParameter("data_dir"))
    }

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val id = req.pathInfo.split("/")[1]
        val fileId = UUID.fromString(id)
        val oFileInfo = fileRepo.findById(fileId)
        if (!oFileInfo.isPresent) {
            resp.writer.println("404")
            return
        }
        val fileInfo = oFileInfo.get()
        resp.setContentLengthLong(fileInfo.size)
        resp.contentType = fileInfo.mimeType
        resp.setHeader("Content-Disposition", "inline; filename=\"${fileInfo.name}\"")
        val path = dataDir.resolve(Paths.get(fileInfo.path))
        Files.copy(path, resp.outputStream)
    }
}