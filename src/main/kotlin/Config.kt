import java.io.FileInputStream
import java.util.*

class Config {

    private val cfgFile = Properties()

    init {
        cfgFile.load(FileInputStream("./cfg.ini"))
    }

    fun getDbConnectionStr(): String = cfgFile.getProperty("db_connection")
    fun getDbPassword(): String = cfgFile.getProperty("db_password")
    fun getDbUsername(): String = cfgFile.getProperty("db_username")
    fun getDataDir(): String = cfgFile.getProperty("data")
    fun getListenPort(): Int? = cfgFile.getProperty("port").toIntOrNull()
}
