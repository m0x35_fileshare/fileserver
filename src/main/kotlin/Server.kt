import org.eclipse.jetty.http.MimeTypes
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.ContextHandler
import org.eclipse.jetty.server.handler.HandlerList
import org.eclipse.jetty.server.handler.ResourceHandler
import org.eclipse.jetty.servlet.ServletHandler
import org.eclipse.jetty.servlet.ServletHolder
import org.eclipse.jetty.server.Server as JettyServer


class Server(private val cfg: Config) {
    private val resourceHandlers = HandlerList()
    private val jetty = JettyServer()

    fun serve() {
        val connector = ServerConnector(jetty)
        connector.port = 8095
        cfg.getListenPort()?.let { connector.port = it }
        jetty.addConnector(connector)

        val servletHandler = ServletHandler()
        val servletHolder = ServletHolder(FileServlet::class.java)
        configure(servletHolder, cfg)

        servletHandler.addServletWithMapping(servletHolder, "/download/*")
        jetty.handler = servletHandler

        jetty.isDumpAfterStart = true;
        jetty.start();
        jetty.join();
    }

    private fun configure(servletHolder: ServletHolder, cfg: Config) {
        servletHolder.setInitParameter("data_dir", cfg.getDataDir())
        servletHolder.setInitParameter("db_connection", cfg.getDbConnectionStr())
        servletHolder.setInitParameter("db_username", cfg.getDbUsername())
        servletHolder.setInitParameter("db_password", cfg.getDbPassword())
    }
}
