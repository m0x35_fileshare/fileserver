import org.sql2o.Sql2o

fun main(args: Array<String>) {
    val cfg = Config()
    val srv = Server(cfg)
    srv.serve()
}
